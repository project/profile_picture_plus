INTRODUCTION
------------

Currently, this module does 2 things:

Allows you to correct the orientation of your user profile picture.
Removes the picture upload element when the user profile picture has already been uploaded
Requires Drupal 7.50 or later.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/profile_picture_plus

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/profile_picture_plus


REQUIREMENTS
------------

This module requires the following version of Drupal core:

Drupal 7.50 or later


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will present orientation options and hide upload element on the profile page.
To restore the default behavior, disable the module and clear caches.


MAINTAINERS
 -----------

 Current maintainers:
  * Lowell Johnson (Lowell) - https://drupal.org/user/92498


